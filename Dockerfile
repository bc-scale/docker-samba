# base image
ARG ARCH=amd64
FROM $ARCH/alpine:3

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/samba" \
  org.label-schema.description="Simple Samba docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/samba" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/docker-samba" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apk --no-cache --no-progress add samba tini

# configure samba
RUN sed -i 's|^;* *\(log file = \).*|   \1/dev/stdout|' /etc/samba/smb.conf

# entrypoint
ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT [ "docker-entrypoint.sh" ]

# default command
ADD samba.sh /usr/local/bin/samba.sh
CMD ["/sbin/tini", "--", "samba.sh"]

EXPOSE 137/udp 138/udp 139/tcp 445/tcp

HEALTHCHECK --interval=60s --timeout=15s \
  CMD smbclient -L \\localhost -U % -m SMB3

# volumes
VOLUME ["/etc", "/var/cache/samba", "/var/lib/samba", "/var/log/samba", "/run/samba"]
