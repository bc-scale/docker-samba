# Samba Docker Image

## Configuration

Create a _smb.conf_ file and mount it to _/etc/samba/smb.conf_.

## Start the container

```bash
docker run -d --restart always \
  -v $(pwd)/smb.conf:/etc/samba/smb.conf \
  -v $(pwd)/share:/share \
  ydkn/samba:latest
```
